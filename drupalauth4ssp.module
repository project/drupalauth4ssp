<?php

/**
 * @file
 * DrupalAuth For simpleSAMLphp module.
 *
 * This module tightly integrates the SimpleSAMLphp Identity Provider login
 * experience with a Drupal site.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implements hook_user_login().
 *
 * Save account ID into the SimpleSAMLphp state, if the user came from IdP.
 */
function drupalauth4ssp_user_login(AccountInterface $account) {
  // If the ReturnTo URL is present, extract state ID from it.
  $returnTo = \Drupal::request()->query->get('ReturnTo');
  /** @var Drupal\drupalauth4ssp\SspHandler $sspHandler */
  $sspHandler = \Drupal::service('drupalauth4ssp.ssp_handler');
  if (!empty($returnTo) && $sspHandler->returnPathIsAllowed($returnTo)) {
    $request = Request::create($returnTo);
    if ($stateId = $request->query->get('State')) {
      $sspHandler->saveIdToStat($account->id(), $stateId);
    }
  }
}

/**
 * Implements hook_user_logout().
 *
 * Expire SimpleSAMLphp session as well.
 */
function drupalauth4ssp_user_logout(AccountInterface $account) {
  /** @var Drupal\drupalauth4ssp\SspHandler $sspHandler */
  $sspHandler = \Drupal::service('drupalauth4ssp.ssp_handler');
  $sspHandler->logout();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drupalauth4ssp_form_user_login_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'drupalauth4ssp_user_login_submit';
}

/**
 * Sets redirect upon successful login.
 */
function drupalauth4ssp_user_login_submit($form, FormStateInterface $form_state) {
  // If the ReturnTo URL is present, send the user to the URL.
  $returnTo = \Drupal::request()->query->get('ReturnTo');
  $sspHandler = \Drupal::service('drupalauth4ssp.ssp_handler');
  if (!empty($returnTo) && $sspHandler->returnPathIsAllowed($returnTo)) {
    $form_state->setRedirectUrl(Url::fromUri($returnTo));
  }
}
