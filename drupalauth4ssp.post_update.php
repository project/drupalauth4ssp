<?php

/**
 * @file
 * Post update hooks for drupalauth4ssp.
 */

// phpcs:ignore
// cspell:ignore authsource userpass

/**
 * Add the authsource configuration parameter.
 */
function drupalauth4ssp_post_update_add_authsource_param() {
  $config = \Drupal::service('config.factory')
    ->getEditable('drupalauth4ssp.settings');
  if (!$config->get('authsource')) {
    $config->set('authsource', 'drupal-userpass')
      ->clear('cookie_name')
      ->save();
  }
}

/**
 * Drop "authsource" configuration parameter.
 */
function drupalauth4ssp_post_update_drop_authsource_param() {
  $config = \Drupal::service('config.factory')
    ->getEditable('drupalauth4ssp.settings');
  $config
    ->clear('authsource')
    ->save();
}

/**
 * Convert "returnto_list" configuration parameter.
 */
function drupalauth4ssp_post_update_convert_returnto_list_param() {
  $config = \Drupal::service('config.factory')
    ->getEditable('drupalauth4ssp.settings');
  $returnToList = array_map('trim', explode(PHP_EOL, $config->get('returnto_list')));
  $config
    ->set('returnto_list', $returnToList)
    ->save();
}
