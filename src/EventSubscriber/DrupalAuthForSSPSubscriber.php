<?php

namespace Drupal\drupalauth4ssp\EventSubscriber;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\drupalauth4ssp\SspHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * DrupalAuth for SimpleSAMLphp event subscriber.
 */
class DrupalAuthForSSPSubscriber implements EventSubscriberInterface {

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * SimpleSamlPHP handler service.
   *
   * @var \Drupal\drupalauth4ssp\SspHandler
   */
  protected $sspHandler;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   Account proxy.
   * @param \Drupal\drupalauth4ssp\SspHandler $sspHandler
   *   SimpleSamlPHP handler service.
   */
  public function __construct(AccountProxyInterface $account_proxy, SspHandler $sspHandler) {
    $this->accountProxy = $account_proxy;
    $this->sspHandler = $sspHandler;
  }

  /**
   * Check if redirect destination is set by the drupalauth4ssp_user_logout().
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Response event.
   */
  public function checkRedirection(ResponseEvent $event): void {
    if ($event->getResponse() instanceof RedirectResponse) {
      $response = $event->getResponse();
      $path = $response->getTargetUrl();
      $frontPage = Url::fromRoute('<front>')->setAbsolute()->toString();

      // Redirect after log out.
      $responseIsHttpFound = $response->getStatusCode() === Response::HTTP_FOUND;
      $isRedirectToFrontPage = ($path === $frontPage && $responseIsHttpFound);
      $destination = &drupal_static('drupalauth4ssp_user_logout');
      if ($isRedirectToFrontPage && !empty($destination)) {
        $response->setTargetUrl($destination);
        $event->stopPropagation();
      }
    }
  }

  /**
   * Save account ID into SimpleSAMLphp state.
   *
   * When user is authenticated by the cookie.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Response event.
   */
  public function saveIdToState(ResponseEvent $event): void {
    if ($event->getResponse() instanceof RedirectResponse) {
      $response = $event->getResponse();
      $responseIsHttpFound = $response->getStatusCode() === Response::HTTP_FOUND;
      $returnTo = $event->getRequest()->query->get('ReturnTo');
      $isLoginRequest = $event->getRequest()->attributes->get('_route') === 'user.login';
      // If this was request to login and user was authenticated by the cookie,
      // and we have a returnTo URL.
      if ($isLoginRequest && $responseIsHttpFound && $returnTo) {
        $request = Request::create($returnTo);
        if ($stateId = $request->query->get('State')) {
          $this->sspHandler->saveIdToStat($this->accountProxy->id(), $stateId);
        }
        $response->setTargetUrl($returnTo);
        $event->stopPropagation();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => [['checkRedirection'], ['saveIdToState']],
    ];
  }

}
